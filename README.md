# :four_leaf_clover: Blip 'n Blup's Skyward Adventures
=========

A Bubble Bobble inspired platform game, developed in Qt.  

For the 3D continuation of this project see [here](https://gitlab.com/luckeyproductions/games/BlipNBlup).